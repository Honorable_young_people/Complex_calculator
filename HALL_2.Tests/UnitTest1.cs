﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace HALL_2.Tests
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod] public void equalNumbers_returnTrueTest()
        { Complexnumber A = new Complexnumber(0, 0);
            Complexnumber B = new Complexnumber(0, 0);
            bool expected = true; bool actual = A.Equal(A, B);
            Assert.AreEqual(expected, actual);
        }
        [TestMethod] public void notEqualNumbers_returnTrueTest()
        { Complexnumber A = new Complexnumber(10, 0);
            Complexnumber B = new Complexnumber(0, 10);
            bool expected = false; bool actual = A.Equal(A, B);
            Assert.AreEqual(expected, actual);
        }
        [TestMethod] public void sumNumbersTest()
        { Complexnumber A = new Complexnumber(5, 0);
            Complexnumber B = new Complexnumber(5, 0);
            Complexnumber expected = new Complexnumber(10, 0);
            Complexnumber actual = A.Sum(A, B);
            Assert.AreEqual(expected.getreal(), actual.getreal());
            Assert.AreEqual(expected.getimaginary(), actual.getimaginary());
        }
        [TestMethod] public void subNumbersTest()
        { Complexnumber A = new Complexnumber(5, 0);
            Complexnumber B = new Complexnumber(5, 0);
            Complexnumber expected = new Complexnumber(0, 0);
            Complexnumber actual = A.Sub(A, B);
            Assert.AreEqual(expected.getreal(), actual.getreal());
            Assert.AreEqual(expected.getimaginary(), actual.getimaginary());
        }
    }
}

